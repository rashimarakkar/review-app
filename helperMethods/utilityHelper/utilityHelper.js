
const Utility = {
    setImageSavePath: (path)=> {
        console.log(path)

        if(path.includes('review-app/public/')){
            var splitPath = path.split('review-app/public/')
           
            return splitPath[1]
            
        }
        else if(path.includes('review-app\\public\\')){
            var splitPath = path.split('review-app\\public\\')
           
            return splitPath[1]
           
        }
        else{
            console.log(`path error`)
        }
    }
}

module.exports = Utility