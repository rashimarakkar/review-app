
const ObjectID = require('mongodb').ObjectID
const db = require('../../config/db/dbConnections')
const dbCollections=require('../../config/db/dbCollections')

const profile = {
    insertNewProfile : (data) => {
        console.log(`inside insertNewProfile`)
        return new Promise((resolve, reject) => {
            db.get()
                .collection(dbCollections.COL_PROFILE)
                .insertOne(profile.dataForNewProfile(data),(err, done)=> {
                    if(done != null) {
                        resolve(true)
                    }
                    else{
                        resolve(false)
                    }
                })
        })
    },
    dataForNewProfile : (data)=> {
        console.log(`inside dataForNewProfile`)
        return doc = {
            name:data.name,
            email:data.email,
            mobile:data.mobile,
            address:data.address,
            image:data.image,
            createdOn : new Date()
            
        }
    },
    getAllProfile : ()=> {
        return new Promise((resolve, reject)=> {
          db.get()
            .collection(dbCollections.COL_PROFILE)
            .aggregate([
              {
                '$project': {
                  'name': 1, 
                  'email': 1, 
                  'mobile': 1, 
                  'address': 1,
                  'image':1
                }
              }
            ])
            .toArray((err, arr)=> {
              if(arr != null){
                resolve(arr)
              }
              else{
                resolve(null)
              }
            })
        })
      },
   
    findById : (id)=> {
       
        return new Promise((resolve, reject)=> {
            db.get()
        .collection(dbCollections.COL_PROFILE)
        .aggregate([
            {
              '$match': {
                '_id': ObjectID(id)
              }
            }, {
              '$project': {
                'createdOn': 0
              }
            }
          ])
          .toArray((err, arr)=> {
            if(arr != null){
                resolve(arr[0])
            }
            else{
                resolve(err)
            }
          })
          })


        
     },
    updateById: (id, data)=> {
           
        return new Promise((resolve, reject)=> {
        db.get()
        .collection(dbCollections.COL_PROFILE).updateOne({_id: ObjectID(id)}, profile.dataForUpdateProfile(data), (err, result)=> {
            
            if(!err){
                resolve(true)
            }
            else{
                resolve(false)
            }
        })
          })



        
    },
    dataForUpdateProfile : (data)=> {
        
        return (
            updateData = {
                $set: {
                    name:data.name,
                    email:data.email,
                    mobile:data.mobile,
                    address:data.address,
                    image:data.image,
                    modifiedOn : new Date()


                    
                }
            }
        )
    },
    deleteById : (id)=> {
   
            return new Promise((resolve, reject)=> {
                db.get()
                .collection(dbCollections.COL_PROFILE)
                .deleteOne({_id: ObjectID(id)}, (err, deleted)=> {
                  if(!err){
                    
                    resolve(true)
                  }
                  else{
                    resolve(false)
                  }
                })
              })
    

    }
}

module.exports = profile