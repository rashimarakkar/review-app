
global.appRoot = require('app-root-path')
const   createError = require('http-errors'),
        express = require('express'),
        path = require('path'),
        cookieParser = require('cookie-parser'),
        logger = require('morgan'),
        db=require('./config/db/dbConnections'),
        dbConfig=require('./config/db/dbCollections'),
        indexRouter = require('./routes/index'),
        usersRouter = require('./routes/users'),
        session = require("express-session"),
        flash = require("connect-flash"),

         app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
  session({
    name: "review-app",
    secret: "mySuperAdminSecretUser",
    resave: true,
    saveUninitialized: true
  })
);
app.use(flash());

app.use('/', indexRouter);
app.use('/users', usersRouter);
db.connect(dbConfig.dbString, err => {
  if (err) {
   console.log("Unable to connect to Signaling");
    
    process.exit(1);
  } else {
   console.log("Signaling Started Successfully....");
  }
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
