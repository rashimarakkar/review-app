const Joi = require('@hapi/joi')

module.exports = Joi.object({
    name: Joi.string().required(),
    mobile: Joi.string().regex(/^[789][0-9]{9}$/).required(),
    address: Joi.string().required(),
    image: Joi.string().required(),
    email: Joi.string().email({minDomainSegments: 2}).required()

    
})