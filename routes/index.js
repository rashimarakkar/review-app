const  express = require('express'),
       router = express.Router(),
       multer = require('multer'),
       utilityHelper=require('../helperMethods/utilityHelper/utilityHelper'),
       profileSchema=require('../models/joi/profileSchema'),
       profileHelper=require('../helperMethods/dbHelperMethods/profileHelper')

/* GET home page. */
// multer is used for file uploading 
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    //debug(req.body)
    cb(null, appRoot+'/public/images/profile/')
     
  },
  filename: (req, file, cb)=> {
      let extArray = file.mimetype.split("/");
      let extension = extArray[extArray.length - 1];

      cb(null, req.body.uname +'-' + Date.now() + '.' + extension)
  }
})

const upload = multer({ storage : storage });

// multer section ends here

// initial page loading
router.get('/',async(req, res, next)=> {
  // model button is created for adding new item
  // display all items in a separate card
  let AlertMessage = req.flash('profileAlert')[0]
  let ErrorMessage = req.flash('profileError')[0]
  
  let profileDetails=await profileHelper.getAllProfile()
  console.log(profileDetails);
  
  res.render('index', { 
    title: 'Review App',
    alert:AlertMessage,
    error:ErrorMessage,
    profileDetails:profileDetails
  });
});
// add new item to db
router.post('/add-new-item',upload.single('photo'),async(req,res,next)=>{
const {uname,email,mobile,address}=req.body
const profileData = {
  name:uname,
  email:email,
  mobile:mobile,
  address:address,
  image : utilityHelper.setImageSavePath(req.file.path)
  
}
const {error, value} = profileSchema.validate(profileData);
    if(!error){
      
        console.log(value);
        
    

        const isInserted= await profileHelper.insertNewProfile(value)
        if(isInserted){
          console.log('successfully inserted');
          
             req.flash('profileAlert', 'successfully Inserted ')
             res.redirect('/')
          }
          else{
           
            
             req.flash('profileError', 'oops!! something went wrong.')
             res.redirect('/')
          }
     }
    else{
       req.flash('profileError',error.message )
       res.redirect('/')

      console.log(error.message);
      
    }

})

// fetched data with id

router.get('/edit-profile/:id',async(req,res,next)=>{
  
let profileById=await profileHelper.findById(req.params.id)
  console.log(profileById);
  if(profileById){
    res.status(200).send(profileById)
  }else{
    res.status(404).send(profileById)
  }
  
  
})
//post edited item
router.post('/edit-profile/:id',upload.single('photo'),async(req,res,next)=>{
  const {uname,email,mobile,address,oldPhoto}=req.body
  const profileData = {
    name:uname,
    email:email,
    mobile:mobile,
    address:address,
  }

  if(!req.file){
       
    profileData.image = oldPhoto
  }
  else{
    profileData.image = utilityHelper.setImageSavePath(req.file.path)
  }
  const {error, value} = profileSchema.validate(profileData);
      if(!error){
        
          const isInserted= await profileHelper.updateById(req.params.id,value)
          if(isInserted){
            
               req.flash('profileAlert', 'Profile successfully Updated ')
               res.redirect('/')
            }
            else{
             
              
               req.flash('profileError', 'oops!! something went wrong.')
               res.redirect('/')
            }
       }
      else{
         req.flash('profileError',error.message )
         res.redirect('/')
  
      }
  
})
// delete item
router.get('/delete-profile/:id',async(req,res,next)=>{
  
  let isDeleted=await profileHelper.deleteById(req.params.id)
    if(isDeleted){
      
             req.flash('profileAlert', 'successfully Deleted ')
             res.redirect('/')
    }else{
      req.flash('profileError', 'oops!! something went wrong.')
      res.redirect('/')
    }
  })
  
  

module.exports = router;
